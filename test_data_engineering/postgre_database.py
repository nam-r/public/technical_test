import logging

import pandas as pd
import psycopg2
import psycopg2.extras

from psycopg2._json import Json
from psycopg2.extensions import register_adapter

register_adapter(dict, Json)


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


class PostgreDatabase:
    """A class allowing to connect and search in a Postgre database. This class
    is a context, that should be used in a `with` statement
    This class is initialized exactly as the connect function in `psycopg2`.

    The connection parameters are be specified as a string:

         PostgreDatabase(f"postgres://{POSTGRES_USER}:"
                         f"{POSTGRES_PASSWORD}@"
                         f"{POSTGRES_HOST}:"
                         f"{POSTGRES_PORT}/"
                         f"{POSTGRES_DB}")

    Parameters
    ----------
    dsn : `str`
        The DSN string for connection
    """

    def __init__(self, dsn: str):
        self.conn = psycopg2.connect(dsn=dsn)
        logging.info("Connected. DSN :%s", self.conn.dsn)
        self.cur = self.conn.cursor()

    def cursor(self):
        """The cursor"""
        return self.cur

    def execute(self, query: str, args=None):
        """Execute a query.

        Parameters
        ----------
        query : `str`
            The query

        args : `tuple` or `dict`
            The list of arguments to pass for execution of the query
        """
        self.cur.execute(query, args)

    def commit(self):
        """Commit changes to the database."""
        self.conn.commit()

    def fetchall(self):
        """Fetch all the results of the executed query"""
        return self.cur.fetchall()

    def execute_and_fetchall(self, query: str):
        """Execute the query and fetch all results."""
        self.execute(query)
        return self.fetchall()

    def does_table_exist(self, schema, table) -> bool:
        """
        Check if a table or view exists in the specified schema of the database.
        """

        result = self.execute_and_fetchall(
            f"""
           SELECT EXISTS(
               (
               SELECT
                   table_name
               FROM information_schema.tables
               WHERE table_name = '{table}'
                   AND table_schema = '{schema}'
                )
                UNION
                (
                SELECT
                    table_name
                FROM information_schema.views
                WHERE table_name = '{table}'
                    AND table_schema = '{schema}'
                )
            );
            """
        )

        return result[0][0]

    def does_column_exist(self, schema, table, column) -> bool:
        """
        Check if a column exists in the specified table and schema of the database.
        """

        result = self.execute_and_fetchall(
            f"""
        SELECT EXISTS
            (SELECT
                column_name
            FROM information_schema.columns
            WHERE table_name = '{table}'
                AND table_schema = '{schema}'
                AND column_name = '{column}'
            ) AS column_table;
        """
        )
        return result[0][0]

    def get_dataframe(self, query: str) -> pd.DataFrame:
        """Return results as pandas `DataFrame`.

        Parameters
        ----------
        query: `str`
            The query

        Returns
        -------
        output : `pd.DataFrame`
            Pandas dataframe containing results
        """
        return pd.read_sql(query, self.conn)

    def insert_dataframe(
        self,
        table: str,
        data: pd.DataFrame,
        schema: str = "public",
    ):
        """Insert a pandas dataframe into a sql table

        Parameters
        ----------
        table: name of the table
        data: dataframe with rows to insert
        schema: schema of the table
        """
        if len(data) > 0:
            df_columns = list(data)
            # create (col1,col2,...)
            columns = ",".join(df_columns)

            # create VALUES('%s', '%s",...) one '%s' per column
            values = "VALUES({})".format(",".join(["%s" for _ in df_columns]))

            # create INSERT INTO table (columns) VALUES('%s',...)
            insert_stmt = "INSERT INTO {}.{} ({}) {}".format(
                schema, table, columns, values
            )

            psycopg2.extras.execute_batch(
                self.cur,
                insert_stmt,
                data.astype(object).where(pd.notnull(data), None).values,
            )
            self.conn.commit()

    def get_table_columns(self, table: str, schema="public"):
        """Get columns list for table
        Parameters
        ----------
        table: name of the table
        schema: schema in which table is located
        """
        query = f"""
                SELECT column_name
                FROM information_schema.columns
                WHERE table_name = '{table}'
                  AND table_schema = '{schema}'
            """

        df = self.get_dataframe(query)
        return df["column_name"].tolist()

    def get_table_columns_types(self, table: str, schema="public"):
        """Get columns types dict for table
        Parameters
        ----------
        table: name of the table
        schema: schema in which table is located
        """
        query = f"""
                SELECT
                    column_name,
                    data_type
                FROM
                    information_schema.columns
                WHERE
                    table_name = '{table}'
                AND table_schema = '{schema}';
            """
        df = self.get_dataframe(query)
        return dict(zip(df["column_name"], df["data_type"]))

    def __enter__(self):
        return self

    def __exit__(self, *excs):
        if self.conn:
            self.conn.close()
            logging.info("Disconnected. DSN : %s", self.conn.dsn)
