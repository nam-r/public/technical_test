import os
from unittest import TestCase

import pandas as pd
import psycopg2
from pandas.testing import assert_frame_equal
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


from test_data_engineering.postgre_database import PostgreDatabase


class TestPostgreDatabase(TestCase):
    db_params = "postgresql://%s:%s@%s:%s/%s" % (
        os.getenv("POSTGRES_USER"),
        os.getenv("POSTGRES_PASSWORD"),
        os.getenv("POSTGRES_HOST"),
        os.getenv("POSTGRES_PORT"),
        os.getenv("POSTGRES_DB"),
    )

    def setUp(self):
        """
        Function initiating the test database with some random values
        """
        import logging
        logging.info(os.listdir())
        pg_db = psycopg2.connect(self.db_params)
        pg_db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        self.psql_cur = pg_db.cursor()

        # Drop and create schema public_test
        self.psql_cur.execute("DROP SCHEMA IF EXISTS public_test CASCADE;")
        self.psql_cur.execute("CREATE SCHEMA public_test;")

        # Drop table test_table if exists
        self.psql_cur.execute("DROP TABLE IF EXISTS public_test.test_table;")

        # Create new test table
        self.psql_cur.execute(
            "CREATE TABLE public_test.test_table("
            "id integer PRIMARY KEY, "
            "value integer NOT NULL);"
        )

        # Load data with pandas dataframe
        df = pd.read_csv(
            "test/data/test_data.csv",
            sep="|",
            dtype=str,
        )

        # Insert data in Postgre
        for row in df.iterrows():
            self.psql_cur.execute(
                "INSERT INTO public_test.test_table VALUES ({},{});".format(
                    row[1]["id"], row[1]["value"]
                )
            )

    def test_get_dataframe(self):
        """
        Function testing if get_dataframe() works
        """
        with PostgreDatabase(self.db_params) as db:
            df_test = db.get_dataframe("SELECT * FROM public_test.test_table")


        df = pd.read_csv("test/data/test_data.csv", sep="|")
        assert_frame_equal(df, df_test)
