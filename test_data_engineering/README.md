# Technical test data engineer

Welcome to test_data_engineering module.
You will be asked to contribute to this module, you are not forced to install the module
and to be able tun run tests, but it can be helpful.

## Installation

Here are steps to be able to run tests in a docker image.

Build docker image with module (run this step each time you update code in the module
to update code in the image)
```commandline
cd test_data_engineering
docker compose up -d --build
```

Build postgres database
```commandline
source postgre_database.sh
```

Run tests
```commandline
docker exec test_data_engineering pytest
```

If tests passed, you successfully managed to install test_data_engineering module!


