"""
test_data_engineering installation module
"""
import os

from setuptools import find_packages, setup

setup(
    name="test_data_engineering",
    version="0.1.0",
    description="Module that brings together tools for data engineers",
    install_requires=open(
        os.path.abspath(os.path.join(os.curdir, "requirements.txt")), "r"
    )
    .read()
    .split("\n"),
    packages=find_packages(),
)
